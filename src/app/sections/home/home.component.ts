import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataStoreService } from 'src/app/core/services/data-store.service';
import { IncompleteAttendance } from '../../core/models/incompleteAttendance.model';
import { RecentActivity } from 'src/app/core/models/recentActivity.model';
import { WorkHours } from 'src/app/core/models/workHours.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent implements OnInit {
  incompleteAttendance$: Observable<IncompleteAttendance[]> = new Observable<IncompleteAttendance[]>();
  recentActivity$: Observable<RecentActivity[]> = new Observable<RecentActivity[]>();
  workHours$: Observable<WorkHours[]> = new Observable<WorkHours[]>();

  constructor(public dataStoreService: DataStoreService) { }

  ngOnInit(): void {
    this.incompleteAttendance$ = this.dataStoreService.incompleteAttendance;
    this.recentActivity$ = this.dataStoreService.recentActivity;
    this.workHours$ = this.dataStoreService.workHours;
    this.recentActivity$.subscribe(response => {
      console.log("Respuesta", response);
    })
  }

  countIncompleteSchedules(incompleteAttendance: any[]): number {
    return incompleteAttendance.reduce((total, date) => total + date.schedule.length, 0);
  }
}
