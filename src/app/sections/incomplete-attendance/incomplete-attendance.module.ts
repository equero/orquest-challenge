import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncompleteAttendanceComponent } from './incomplete-attendance.component';



@NgModule({
  declarations: [
    IncompleteAttendanceComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IncompleteAttendanceComponent
  ]
})
export class IncompleteAttendanceModule { }
