import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncompleteAttendanceComponent } from './incomplete-attendance.component';

describe('IncompleteAttendanceComponent', () => {
  let component: IncompleteAttendanceComponent;
  let fixture: ComponentFixture<IncompleteAttendanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IncompleteAttendanceComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IncompleteAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
