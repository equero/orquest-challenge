import { LOCALE_ID, NgModule } from '@angular/core';

import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ApiInterceptor } from './core/interceptors/api-interceptor';
import { HeaderModule } from './header/header.module';
import { NavigationModule } from './navigation/navigation.module';
import { HomeModule } from './sections/home/home.module';
import { IncompleteAttendanceModule } from './sections/incomplete-attendance/incomplete-attendance.module';
import { ScheduleModule } from './sections/schedule/schedule.module';
import { RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    HeaderModule,
    NavigationModule,
    HomeModule,
    IncompleteAttendanceModule,
    ScheduleModule
  ],
  providers: [
    provideClientHydration(),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    {provide: LOCALE_ID, useValue: 'es'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
