import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { catchError, map, mergeMap, toArray } from 'rxjs/operators';
import { DataService } from './data.service';
import { Employee } from '../models/employee.model';
import { WorkDate } from '../models/workDate.model';
import { IncompleteAttendance } from '../models/incompleteAttendance.model';
import { RecentActivity } from '../models/recentActivity.model';
import { WorkHours } from '../models/workHours.model';

@Injectable({
  providedIn: 'root'
})
export class DataStoreService {
  private _employees$: Observable<Employee[]> = of([]);
  private _dates$: Observable<WorkDate[]> = of([]);
  private _incompleteAttendance$: Observable<IncompleteAttendance[]> = of([]);
  private _recentActivity$: Observable<RecentActivity[]> = of([]);
  private _workHours$: Observable<WorkHours[]> = of([]);

  constructor(private dataService: DataService) {
    this.getData();
  }

  private getData(): void {
    this.dataService.getData().pipe(
      catchError((error: any) => {
        console.error('Error fetching data:', error);
        return new Observable<Employee[]>(); // Emite un observable vacío en caso de error
      }),
      map((response: any) => {
        return {
          employees: response.employees,
          dates: response.dates
        };
      })
    ).subscribe((data: any) => {
      this._employees$ = of(data.employees);
      this._dates$ = of(data.dates);
      this._incompleteAttendance$ = this.findIncompleteAttendance(data.dates, data.employees);
      this._recentActivity$ = this.findRecentActivity(data.dates, data.employees);
      this._workHours$ = this.calculateWorkHours(data.dates, data.employees);
    });
  }

  private findRecentActivity(dates: WorkDate[], employees: Employee[]): Observable<RecentActivity[]> {
    return from(dates).pipe(
      mergeMap(date =>
        from(date.schedule).pipe(
          map(schedule => {
            const employee = employees.find(emp => emp.id === schedule.employeeId);
            const employeeName = employee ? `${employee.name} ${employee.lastName}` : 'Unknown';
            const type: 'start' | 'end' = schedule.endTime ? 'end' : 'start';
            return {
              date: date.date,
              time: schedule.endTime ? schedule.endTime : schedule.startTime,
              employeeId: schedule.employeeId,
              employeeName,
              type
            };
          })
        )
      ),
      toArray(),
      map(allActivities => 
        allActivities.sort((a, b) => {
          const dateA = new Date(`${a.date}T${a.time}`);
          const dateB = new Date(`${b.date}T${b.time}`);
          return dateB.getTime() - dateA.getTime();
        }).reverse().slice(0, 10)
      )
    );
  }

  private getStartAndEndDateFromWeekNumber(weekNumber: number, year: number): { startDate: Date, endDate: Date } {
    const janFirst = new Date(year, 0, 1);
    const daysOffset = (weekNumber - 1) * 7 - janFirst.getDay() + 1; // Ajuste para comenzar la semana en lunes
    const startDate = new Date(janFirst.setDate(daysOffset));
    const endDate = new Date(startDate.getTime());
  
    // Ajuste endDate al domingo de la misma semana
    endDate.setDate(startDate.getDate() + 6);
  
    return { startDate, endDate };
  }

  private calculateWorkHours(dates: WorkDate[], employees: Employee[]): Observable<WorkHours[]> {
    return from(dates).pipe(
      mergeMap(date => from(date.schedule).pipe(
        map(schedule => {
          if (schedule.startTime && schedule.endTime) {
            const employee = employees.find(emp => emp.id === schedule.employeeId);
            const employeeName = employee ? `${employee.name} ${employee.lastName}` : 'Unknown';
  
            const start = new Date(schedule.startTime);
            const end = new Date(schedule.endTime);
            const hoursWorked = (end.getTime() - start.getTime()) / (1000 * 60 * 60);
            const weekNumber = this.getWeekNumber(start);
            
            const { startDate, endDate } = this.getStartAndEndDateFromWeekNumber(weekNumber, start.getFullYear());
  
            return {
              employeeId: schedule.employeeId,
              employeeName: employeeName,
              weekNumber: weekNumber,
              hoursWorked: hoursWorked,
              startDate: `${startDate.getDate()}/${startDate.getMonth() + 1}/${startDate.getFullYear()}`,
              endDate: `${endDate.getDate()}/${endDate.getMonth() + 1}/${endDate.getFullYear()}`,
              date: date.date
            };
          } else {
            return null;
          }
        })
      )),
      toArray(),
      map(schedules => {
        const groupedWorkHours: WorkHours[] = [];
  
        schedules.forEach(schedule => {
          if (schedule) {
            let employeeEntry = groupedWorkHours.find(entry => entry.employeeId === schedule.employeeId);
            if (!employeeEntry) {
              employeeEntry = {
                employeeId: schedule.employeeId,
                employeeName: schedule.employeeName,
                weeklyHours: []
              };
              groupedWorkHours.push(employeeEntry);
            }
  
            let weeklyEntry = employeeEntry.weeklyHours.find(entry => entry.weekNumber === schedule.weekNumber);
            if (!weeklyEntry) {
              weeklyEntry = {
                weekNumber: schedule.weekNumber,
                hoursWorked: 0,
                startDate: schedule.startDate,
                endDate: schedule.endDate,
                daysWorked: []
              };
              employeeEntry.weeklyHours.push(weeklyEntry);
            }
  
            weeklyEntry.hoursWorked += schedule.hoursWorked;
  
            const dayEntry = weeklyEntry.daysWorked.find(entry => entry.date === schedule.date);
            if (dayEntry) {
              dayEntry.hoursWorked += schedule.hoursWorked;
            } else {
              weeklyEntry.daysWorked.push({
                date: schedule.date,
                hoursWorked: schedule.hoursWorked
              });
            }
          }
        });
  
        return groupedWorkHours;
      })
    );
  }
  
  
  private getWeekNumber(date: Date): number {
    const firstDayOfYear = new Date(date.getFullYear(), 0, 1);
    const pastDaysOfYear = (date.getTime() - firstDayOfYear.getTime()) / (24 * 3600 * 1000);
    return Math.ceil((pastDaysOfYear + firstDayOfYear.getDay() + 1) / 7);
  }
  
  private findIncompleteAttendance(dates: WorkDate[], employees: Employee[]): Observable<IncompleteAttendance[]> {
    const incompleteAttendance: IncompleteAttendance[] = dates
      .map(date => {
        const incompleteSchedules = date.schedule.filter(schedule => !schedule.startTime || !schedule.endTime);
        const schedulesWithEmployeeNames = incompleteSchedules.map(schedule => {
          const employee = employees.find(emp => emp.id === schedule.employeeId);
          const employeeName = employee ? `${employee.name} ${employee.lastName}` : 'Unknown';
          return { ...schedule, employeeName };
        });
        return { date: date.date, schedule: schedulesWithEmployeeNames };
      })
      .filter(date => date.schedule.length > 0);
    return of(incompleteAttendance);
  }

  get employees$(): Observable<Employee[]> {
    return this._employees$;
  }

  get dates$(): Observable<WorkDate[]> {
    return this._dates$;
  }

  get incompleteAttendance(): Observable<IncompleteAttendance[]> {
    return this._incompleteAttendance$;
  }

  get recentActivity(): Observable<RecentActivity[]> {
    return this._recentActivity$;
  }

  get workHours(): Observable<WorkHours[]> {
    return this._workHours$;
  }
}