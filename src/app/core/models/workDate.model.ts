export interface WorkDate {
    date: string;
    schedule: Schedule[];
}
  
interface Schedule {
    employeeId: number;
    startTime: string;
    endTime?: string;
}