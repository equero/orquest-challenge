export interface IncompleteAttendance {
    date: string;
    schedule: Schedule[];
}
  
interface Schedule {
    employeeId: number;
    employeeName: string;
    startTime?: string;
    endTime?: string;
}