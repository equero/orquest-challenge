export interface RecentActivity {
    date: string;
    type: 'start' | 'end';
    time: string
    employeeId: number;
    employeeName: string;
}