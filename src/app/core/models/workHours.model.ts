export interface WorkHours {
    employeeId: number;
    employeeName: string;
    weeklyHours: WeeklyHours[]; // Agrega esta línea
  }
  
  export interface WeeklyHours {
    weekNumber: number;
    hoursWorked: number;
    startDate: string;
    endDate: string;
    daysWorked: DailyHours[];
  }
  
  export interface DailyHours {
    date: string;
    hoursWorked: number;
  }