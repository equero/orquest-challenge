import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './sections/home/home.component';
import { IncompleteAttendanceComponent } from './sections/incomplete-attendance/incomplete-attendance.component';
import { ScheduleComponent } from './sections/schedule/schedule.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'incomplete-attendance', component: IncompleteAttendanceComponent },
  { path: 'schedule', component: ScheduleComponent },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }