export const environment = {
    production: false,
    useFakeAPI: true,
    apiUrl: 'https://api.example.com'
};
