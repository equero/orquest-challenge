export const environment = {
    production: true,
    useFakeAPI: false,
    apiUrl: 'https://api.example.com'
};
