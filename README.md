# Orquest Challenge

## Instalar y Ejecutar

Para instalar y ejecutar el proyecto, sigue estos pasos:

1. Clona este repositorio en tu máquina local.
2. Instala las dependencias del proyecto ejecutando `npm install`.
3. Inicia la aplicación con el comando `ng serve -c development`.
4. Abre tu navegador e ingresa a `http://localhost:4200` para ver la aplicación en acción.

## Consideraciones al Revisar

Al revisar este proyecto, por favor considera lo siguiente:

- Desafortunadamente, la prueba ha superado mi tiempo asignado. Originalmente planeaba dedicarle más horas, pero solo pude trabajar durante aproximadamente 8 horas. Esta semana he tenido varios problemas personales con trámites y viajes.
- Aunque el código es funcional, el aspecto visual no está completo. Solo he agregado una apariencia visual básica para facilitar la revisión sin sacrificar demasiado tiempo.
- He intentado usar RxJS siempre que ha sido posible. Sin embargo, no he utilizado esta biblioteca en un par de años, y algunos de los operadores que solía usar han sido deprecados.

## Funcionalidades Completas ✅

- [x] Ver el número de horas trabajadas cada día y semana.
- [x] Ver el detalle de cada fichaje.
- [x] Indicación visual de fichajes incompletos.
- [x] Código reactivo.

## Funcionalidades Incompletas ❌

- [ ] Pruebas unitarias. Desafortunadamente, no pude completar esta parte debido a limitaciones en mi entorno de desarrollo actual. Actualmente estoy en un viaje debido a un problema familiar, y todo lo que tengo disponible para trabajar es una Steam Deck. Configuré Arch Linux en la Steam Deck para trabajar con un teclado y ratón en un monitor externo. Sin embargo, Arch Linux en Steam Deck es un sistema operativo de solo lectura y no permite ejecutar Chrome nativamente. ng test utiliza Chromium para ejecutar las pruebas unitarias, y como resultado, no he podido completar esta sección.
  
  Aunque las pruebas no eran complicadas, la mayoría de ellas consistían en proporcionar datos de entrada y verificar la salida.

## Comentarios sobre la Prueba

La prueba ha sido desafiante pero positiva. Me ha permitido recordar y aprender muchas cosas nuevas. Una de las principales dificultades fue aprender a usar la reactividad directamente en el HTML, ya que durante la primera entrevista se hizo hincapié en si alguna vez había utilizado este enfoque, y desafortunadamente, no tenía experiencia previa con él. Aunque reconozco que esto no es una excusa, durante esta semana he tenido que trabajar desde una Steam Deck debido a un problema familiar. La mayor parte del tiempo se ha dedicado a configurar la consola para ejecutar el código, y apenas he podido trabajar durante aproximadamente 8-12 horas en total.

A pesar de las dificultades, considero que la prueba ha sido muy valiosa, y me gustaría dedicar más tiempo a pulir y completar las secciones que había planeado inicialmente.

